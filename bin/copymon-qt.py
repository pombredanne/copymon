#!/usr/bin/python

# Copyright (c) 2013, Lior Amar and Gal Oren (liororama@gmail.com | galoren.com@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar or Gal Oren, nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import time
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import  QObject, pyqtSignal

# The gui ui definitions
import copymon_qt_ui


from copymonitor import *


class GenericThread(QtCore.QThread):
 def __init__(self, function, *args, **kwargs):
  QtCore.QThread.__init__(self)
  self.function = function
  self.args = args
  self.kwargs = kwargs

 def __del__(self):
  self.wait()

 def run(self):
  self.function(*self.args,**self.kwargs)
  return

class GuiProgress(ProgressBar.IProgressBar):
	
	def __init__(self, copy_gui):
		self.beginning_time = time.time()
		self.total_files = 0
		self.total_size = 0
		self.copied_files = 0
		self.copied_size = 0
		self.copy_gui = copy_gui

	def update_scan_progress(self, current_files_found, current_total_size_mb):
		""" Updating the scan progress """
		self.total_files = current_files_found
		self.total_size = current_total_size_mb
		
		print "Update progress", self.total_files
		
		self.copy_gui.scan_update_sig.emit({"files": self.total_files , "size" : self.total_size})

	def get_copy_update_limits(self):
		""" Return (file_count, mb_count) to be used for update the progress object """
		return (100, 10)

	def update_copy_progress(self, current_files_copied, current_total_size_copied_mb):

		self.copied_size = current_total_size_copied_mb
		self.copied_files = current_files_copied
		
		#print "Total {0} copied {1}".format(self.total_files, self.copied_files)
		#percent = float(self.copied_size_mb/ float(self.total_size_mb))
		current_time = time.time()
		
		# Emmiting the update signal only if more than xxx time has passed
		if (current_time - self.beginning_time > 0.01) or (self.copied_files == self.total_files) :
			self.copy_gui.copy_update_sig.emit({"files" : self.copied_files, "size" : self.copied_size})	
			self.beginning_time = current_time


	def done_copy(self, file_count, size_count, status):
		self.beginning_time = 0
		self.update_copy_progress(file_count, size_count)
    		
	def report_error(self, error_message):
		""" Error reporting """


class CopymonGui(QtGui.QMainWindow):
	
	# Defining the update signal
	scan_update_sig = QtCore.pyqtSignal(dict)
	copy_update_sig = QtCore.pyqtSignal(dict)
	copy_done_sig = QtCore.pyqtSignal(bool)
	
	def __init__(self, parent=None, verbose=False):
		# Important line
		QtGui.QWidget.__init__(self, parent)
		
		self.verbose = verbose
		self.cp_cmd = "cp"
		self.ui = copymon_qt_ui.Ui_Dialog()
		self.ui.setupUi(self)

		self.progress_obj = GuiProgress(self)
		
		self.copy_pool = []
		self.is_copy_started = False
	
		# Set progress bar initial values
		self.ui.progressbar_files.setMinimum(0)
		self.ui.progressbar_files.setMaximum(100)
		self.ui.progressbar_files.setValue(0)  
		
		self.ui.progressbar_size.setMinimum(0)
		self.ui.progressbar_size.setMaximum(100)
		self.ui.progressbar_size.setValue(0)  
		
		
		# Commect buttons
		self.ui.btn_start.clicked.connect(self.btn_start_clicked)
		self.ui.btn_cancel.clicked.connect(self.btn_cancel_clicked)
		
		
		self.source_dir = "/tmp/d1"
		self.dest_dir = "/tmp/d2"
	
		self.ui.lbl_source_name.setText(self.source_dir)
		self.ui.lbl_dest_name.setText(self.dest_dir)
		
		self.total_files = 0
		self.total_size = 0
		self.copied_files = 0
		self.copied_size = 0
		
	
	def copy_start(self, name):
		""" This is the work method which runs in a seperate thread """
		
		
		# Scan stage
		metadata_scanner = MetadataScanner.MetadataScanner(self.source_dir, progress_obj=self.progress_obj, verbose=True)
		res = metadata_scanner.scan(update_count=100)
		if not res:
			if options.verbose:
				print "Aborting copy due to errors in scan"
			
			# TODO print error on window
			return
			

		# Copy stage
		# Creating the specific copy monitor object.
		copy_obj = CopyMonitor.CopyMonitorFactory(self.cp_cmd,
												  metadata_scanner,
												  self.source_dir, self.dest_dir,
												  self.progress_obj, self.verbose)

		res = copy_obj.copy_dir()
		if not res:
			self.error_list = copy_obj.error_list
			
		if self.verbose: print "copy_start : done "
		self.copy_done_sig.emit(res)
		
	def scan_update_progress(self, progress_dict):
		""" Updating the scan the progress bar should look different """
		
		self.total_files = progress_dict['files']
		self.total_size = progress_dict['size']
		
		#self.ui.progressbar_files.setValue(progress_dict['files'])  
		#self.ui.progressbar_size.setValue(progress_dict['size'])  
		
		self.ui.lbl_files_info.setText("Scanning: {0} files detected".format(self.total_files))
		self.ui.lbl_size_info.setText("Scanning: {0:.2f} MB detected".format(self.total_size))
		
		
	def copy_update_progress(self, progress_dict):
		self.copied_files = progress_dict['files']
		self.copied_size = progress_dict['size']
		
		self.ui.lbl_files_info.setText("Copying: {0}/{1} files copied".format(self.copied_files, self.total_files))
		self.ui.lbl_size_info.setText("Copying: {0:.2f}/{1:.2f} MB copied".format(self.copied_size, self.total_size))
		
		files_percent = float(self.copied_files) / float(self.total_files) * 100.0
		self.ui.progressbar_files.setValue(files_percent)

		size_percent = float(self.copied_size) / float(self.total_size) * 100.0
		self.ui.progressbar_size.setValue(size_percent)

		
	def copy_done(self, status):
		""" Copy operation is done - final update of info"""
		if self.verbose: print "copy_done() ----> called "
		self.copied_files = self.progress_obj.copied_files
		self.copied_size = self.progress_obj.copied_size
		self.copy_update_sig.emit({"files" : self.copied_files, "size" : self.copied_size})	
		
		if not status:
			self.ui.lbl_status.setText("Error(s) detected in copy")
			err_str = ""
			line_number = 0
			for error in self.error_list:
				err_str += "{0} File: {1}     : {2}".format(line_number, error[0],error[1])
				line_number += 1		
			self.errorMessageDialog = QtGui.QErrorMessage(self)
			self.errorMessageDialog.showMessage(err_str)
		
		
	def btn_start_clicked(self):
		print "Button start clicked" 
		
		if self.is_copy_started:
			if self.verbose: print "Copy already started"
			return
		self.is_copy_started = True
		
		# generic thread using signal
		if self.verbose : print "Adding thread"
		self.copy_pool.append( GenericThread(self.copy_start, "from generic thread using signal ") )
		self.disconnect( self, QtCore.SIGNAL("copy_done(QString)"), self.copy_done )
		
		#self.connect( self, QtCore.SIGNAL("copy_done(QString)"), self.copy_done )
		self.scan_update_sig.connect(self.scan_update_progress)
		self.copy_update_sig.connect(self.copy_update_progress)
		self.copy_done_sig.connect(self.copy_done)
		#self.connect( self, QtCore.SIGNAL("update_progress(int)"), self.update_progress )
		#self.connect( self, QtCore.SIGNAL("update_progress(int)"), self.update_progress )
	
	
		self.copy_pool[len(self.copy_pool)-1].start()

	def btn_cancel_clicked(self):
		QtGui.QApplication.quit()

app = QtGui.QApplication(sys.argv)

myapp = CopymonGui(verbose=True)
myapp.show()
sys.exit(app.exec_())
