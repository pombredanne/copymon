"""
This module provides the progress bar interface class and a simple terminal
implementation. The ProgressBar class is to be implemented by the user of the
copymon package which want to obtain information about the progress of the copy
"""
# Copyright (c) 2013, Lior Amar and Gal Oren (liororama@gmail.com | galoren.com@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar or Gal Oren, nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import time


class IProgressBar(object):
    """
    An interface for performing progress
    """

    def __init__(self):
        """
        The members of this class are the information updated by the copymon
        package. The user of this class should implement the abstract methods which
        are called from time to time when enough progress was done.
        """
        self.total_size_mb = 0
        self.copied_size_mb = 0
        self.total_files = 0
        self.copied_files = 0
        self.start_time = None
        self.title = None

    def set_title(self, title):
        self.title = title

    def set_total_size_mb(self, total_size_mb):
        self.total_size_mb = total_size_mb

    def set_total_files(self, total_files):
        self.total_files = total_files

    def update_scan_progress(self, current_files_found, current_total_size_mb):
        raise NotImplementedError("Should have implemented this" )

    def get_copy_update_limits(self):
        """ Return (file_count, mb_count) to be used for update the progress object """
        raise NotImplementedError("Should have implemented this")

    def update_copy_progress(self, current_files_copied, current_total_size_copied_mb):
        raise NotImplementedError("Should have implemented this" )

    def done_copy(self, file_count, size_count, status):
        """ Final update """
        raise NotImplementedError("Should have implemented this")

    def report_error(self, error_message):
        raise NotImplementedError("Should have implemented this" )


import sys


class TerminalProgressBar(IProgressBar):
    """
    A simple terminal progress bar

    Implementing a simple progress bar in terminal. This is a one line progress bar
    """
    @classmethod
    def is_progressbar_for(cls, name):
        return name == "terminal"

    def __init__(self, bar_len=80):
        super(TerminalProgressBar,self).__init__()
        self.bar_len = bar_len
        self.beginning_time = time.time()

    def update_scan_progress(self, current_files_found, current_total_size_mb):
        sys.stdout.write("\r{0} files scanned  ({1:.2f} MB)".format(current_files_found, current_total_size_mb))
        sys.stdout.flush()

    def get_copy_update_limits(self) :
        return (100, 10)

    def update_copy_progress(self, current_files_copied, current_total_size_copied_mb):

        self.copied_size_mb = current_total_size_copied_mb
        self.current_files_copied = current_files_copied

        current_time = time.time()
        if current_time - self.beginning_time < 0.1:
            return

        if self.total_size_mb > 0:
            percent = float(self.copied_size_mb/ float(self.total_size_mb))
        else:
            percent = 1
        hashes = '#' * int(round(percent * self.bar_len))
        spaces = ' ' * (self.bar_len - len(hashes))

        bar_text         = hashes + spaces
        percent_text     = int(round(percent * 100))
        size_text        = int(self.copied_size_mb)
        total_size_text  = int(self.total_size_mb)

        title = self.title
        if title is None:
            title = "Copy..."

        s = "\r{0} [{1}] {2}% ({3:.2f}/{4:.2f} MB | {5}/{6} Files)".format(title,
                                                                       bar_text,
                                                                       percent_text,
                                                                       size_text,
                                                                       total_size_text,
                                                                       self.current_files_copied,
                                                                       self.total_files)

        sys.stdout.write(s)
        sys.stdout.flush()
        self.beginning_time = time.time()

    def done_copy(self, file_count, size_count, status):
        self.beginning_time = 0
        self.update_copy_progress(file_count, size_count)

    def report_error(self, error_message):
        sys.stdout.write("ERROR: {0} ".format(error_message))
        sys.stdout.write("   ---->   Copy Aborted !!!")


class ColorTerminalProgressBar(IProgressBar):
    """
    Implementing a simple progress bar in terminal. This is a one line progress bar
    """
    @classmethod
    def is_progressbar_for(cls, name):
        return name == "color-terminal"

    BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)

    RESET_SEQ = "\033[0m"
    COLOR_SEQ = "\033[1;%dm"
    BOLD_SEQ = "\033[1m"

    COLORS = {
            'RED'          : RED,
            'GREEN'        : GREEN,
            'YELLOW'       : YELLOW,
            'BLUE'         : BLUE,
            'MAGENTA'      : MAGENTA,
            'CYAN'         : CYAN,
            'WHITE'        : WHITE,
            'DEBUG_R'      : RED,
            'DEBUG_G'      : GREEN,
            'DEBUG_B'      : BLUE,
            'DEBUG_Y'      : YELLOW,
            'DEBUG_C'      : CYAN,
            'DEBUG_M'      : MAGENTA
        }

    def __init__(self, bar_len=80):
        super(ColorTerminalProgressBar,self).__init__()

        self.bar_len = bar_len
        self.beginning_time = time.time()

    def update_scan_progress(self, current_files_found, current_total_size_mb):
        sys.stdout.write("\r{0} files scanned  ({1:.2f} MB)".format(current_files_found, current_total_size_mb))
        sys.stdout.flush()

    def get_copy_update_limits(self) :
        return (100, 10)

    def update_copy_progress(self, current_files_copied, current_total_size_copied_mb):

        self.copied_size_mb = current_total_size_copied_mb
        self.current_files_copied = current_files_copied

        if self.total_size_mb > 0:
            percent = float(self.copied_size_mb / float(self.total_size_mb))
        else:
            percent = 1

        hashes = '#' * int(round(percent * self.bar_len))
        spaces = ' ' * (self.bar_len - len(hashes))

        bar_text = hashes + spaces
        percent_text = int(round(percent * 100))
        size_text = int(self.copied_size_mb)
        total_size_text = int(self.total_size_mb)

        color_num = 30 + self.COLORS['GREEN']
        bar_text = self.COLOR_SEQ % color_num + bar_text + self.RESET_SEQ

        color_num = 30 + self.COLORS['BLUE']
        size_text = self.COLOR_SEQ % color_num + "({0:.2f}/{1:.2f} MB)".format(size_text, total_size_text) + self.RESET_SEQ

        #if int(current_total_size_copied_mb) % (1 + int(float(self.total_size_mb) / float(self.bar_len))) < 5:

        current_time = time.time()

        if current_time - self.beginning_time > 0.01:  # need to check if its is the last update
            title = self.title
            if title is None:
                title = "Copy..."

            s = "\r{0} [{1}] {2}% ({3:.2f}/{4:.2f} MB | {5}/{6} Files | Errors: {7})".format(title,
                                                                                             bar_text,
                                                                                            percent_text,
                                                                                            size_text,
                                                                                            total_size_text,
                                                                                            self.current_files_copied,
                                                                                            self.total_files,
                                                                                            current_count_error)
            sys.stdout.write(s)
            sys.stdout.flush()
            self.beginning_time = time.time()

    def done_copy(self, file_count, size_count, status):
        self.beginning_time = 0
        self.update_copy_progress(file_count, size_count)

    def report_error(self, error_message):
        sys.stdout.write("ERROR: {0} ".format(error_message))
        sys.stdout.write("   ---->   Copy Aborted !!!")


def ProgressBarFactory(name, *args, **kwargs):
    for cls in IProgressBar.__subclasses__():
        if cls.is_progressbar_for(name):
            return cls(*args, **kwargs)
    raise ValueError
